# oci-quickstart-hortonworks
These are Terraform modules that deploy [Hortonworks](https://hortonworks.com/products/) on [Oracle Cloud Infrastructure (OCI)](https://cloud.oracle.com/en_US/cloud-infrastructure).  They are developed jointly by Oracle and Cloudera. For instructions on how to use this material and details on getting support from the vendor that maintains this material, please contact them directly.

* [Hortonworks Data Platform](hdp)
* [Hortonworks Data Framework](hdf)
